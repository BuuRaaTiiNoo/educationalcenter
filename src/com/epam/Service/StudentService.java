package com.epam.Service;

import com.epam.DTO.Student;

import java.util.List;

public interface StudentService {
    public void addStudent(Student student);

    public int getTimeLeft(Student student);

    public int durationCourse(Student student);

    public double middleMark(Student student);

    public double futureStudent(Student student);

    public void checkStatusStudent(Student student);

    public List<Student> filterStudent(List<Student> students);

    public List<Student> sortedStudentByMiddleMark(List<Student> students);

    public List<Student> sortedStudentByTimeLeft(List<Student> students);

    public List<Student> getAllStudents();

    public void checkStatusStudents();

    public void getSortedListStudentByRating();

    public void getSortedListStudentByTimeLeft();

    public void getFilterStudent();
}
