package com.epam.Service;

import com.epam.DTO.Course;

import java.util.List;

public interface CourseService {

    public void addCourses(List<Course> courses);

    public List<Course> getCourses();
}
