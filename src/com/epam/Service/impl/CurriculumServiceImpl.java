package com.epam.Service.impl;

import com.epam.DTO.Curriculum;
import com.epam.Repository.CurriculumRepository;
import com.epam.Repository.impl.CurriculumRepositoryImpl;
import com.epam.Service.CurriculumService;

import java.util.List;

public class CurriculumServiceImpl implements CurriculumService {

    private CurriculumRepository curriculumRepositoryImpl = new CurriculumRepositoryImpl();

    @Override
    public void createCurriculum(Curriculum curriculum) {
        curriculumRepositoryImpl.createCurriculum(curriculum);
    }

    @Override
    public List<Curriculum> getAllCurriculum() {
        return curriculumRepositoryImpl.getCurriculumList();
    }
}
