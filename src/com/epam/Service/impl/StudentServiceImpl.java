package com.epam.Service.impl;

import com.epam.DTO.Course;
import com.epam.DTO.Student;
import com.epam.Repository.StudentRepository;
import com.epam.Repository.impl.StudentRepositoryImpl;
import com.epam.Service.StudentService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;


public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepositoryImpl = new StudentRepositoryImpl();

    @Override
    public void addStudent(Student student) {
        studentRepositoryImpl.addStudent(student);
    }

    @Override
    public int getTimeLeft(Student student) {
        return durationCourse(student) - student.getMarks().size();
    }

    @Override
    public int durationCourse(Student student) {
        int duration = 0;
        for (Course course : student.getCurriculum().getCourses()) {
            duration += course.getDuration();
        }
        return duration / 8;
    }

    @Override
    public double middleMark(Student student) {
        OptionalDouble optionalDouble = student
                .getMarks()
                .stream()
                .mapToInt((s) -> Integer.parseInt(s.toString()))
                .average();
        if (optionalDouble.isPresent()) {
            return new BigDecimal(optionalDouble.getAsDouble()).setScale(2, RoundingMode.UP).doubleValue();
        } else return 0;
    }

    @Override
    public double futureStudent(Student student) {
        int sumMarks = student
                .getMarks()
                .stream()
                .mapToInt((s) -> Integer.parseInt(s.toString()))
                .sum();
        return (sumMarks + 5 * getTimeLeft(student)) / (double) durationCourse(student);
    }

    @Override
    public void checkStatusStudent(Student student) {
        if (OptionalDouble.of(futureStudent(student)).isPresent()) {
            if (futureStudent(student) * 100 <= 4.5 * 100) {
                studentRepositoryImpl.updateStatusStudent(student);
            }
        }
    }

    @Override
    public List<Student> filterStudent(List<Student> students) {
        return students
                .stream()
                .filter(student -> !student.getStatus().equals("Отчислить"))
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> sortedStudentByMiddleMark(List<Student> students) {
        return students
                .stream()
                .sorted(Comparator.comparingDouble(this::middleMark))
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> sortedStudentByTimeLeft(List<Student> students) {
        return students
                .stream()
                .sorted(Comparator.comparingDouble(this::getTimeLeft))
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepositoryImpl.getStudents();
    }

    @Override
    public void checkStatusStudents() {
        for (Student student : getAllStudents()) {
            checkStatusStudent(student);
        }
    }

    @Override
    public void getSortedListStudentByRating() {
        sortedStudentByMiddleMark(getAllStudents()).forEach(System.out::println);
    }

    @Override
    public void getSortedListStudentByTimeLeft() {
        sortedStudentByTimeLeft(getAllStudents()).forEach(System.out::println);
    }

    @Override
    public void getFilterStudent() {
        filterStudent(getAllStudents()).forEach(System.out::println);
    }

}
