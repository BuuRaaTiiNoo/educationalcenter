package com.epam.Service.impl;

import com.epam.DTO.Course;
import com.epam.Repository.CourseRepository;
import com.epam.Repository.impl.CourseRepositoryImpl;
import com.epam.Service.CourseService;

import java.util.List;

public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepositoryImpl = new CourseRepositoryImpl();

    @Override
    public void addCourses(List<Course> courses) {
        for (Course course: courses) {
            courseRepositoryImpl.addCourses(course);
        }
    }

    @Override
    public List<Course> getCourses() {
        return courseRepositoryImpl.getCourses();
    }
}
