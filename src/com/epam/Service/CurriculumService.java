package com.epam.Service;

import com.epam.DTO.Curriculum;

import java.util.List;

public interface CurriculumService {

    public void createCurriculum(Curriculum curriculum);

    public List<Curriculum> getAllCurriculum();
}
