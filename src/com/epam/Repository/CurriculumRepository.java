package com.epam.Repository;

import com.epam.DTO.Curriculum;

import java.util.List;

public interface CurriculumRepository {


    public void createCurriculum(Curriculum curriculum);

    public List<Curriculum> getCurriculumList();
}
