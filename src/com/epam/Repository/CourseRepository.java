package com.epam.Repository;

import com.epam.DTO.Course;

import java.util.List;

public interface CourseRepository {

    public void addCourses(Course course);

    public List<Course> getCourses();
}
