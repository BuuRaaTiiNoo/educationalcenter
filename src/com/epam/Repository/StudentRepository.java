package com.epam.Repository;

import com.epam.DTO.Student;

import java.util.List;

public interface StudentRepository {

    public void addStudent(Student student);

    public void updateStatusStudent(Student student);

    public List<Student> getStudents();
}
