package com.epam.Repository.impl;

import com.epam.DTO.Course;
import com.epam.Repository.CourseRepository;

import java.util.ArrayList;
import java.util.List;

public class CourseRepositoryImpl implements CourseRepository {

    private List<Course> courses = new ArrayList<>();

    @Override
    public void addCourses(Course course){
        courses.add(course);
    }

    @Override
    public List<Course> getCourses() {
        return courses;
    }
}
