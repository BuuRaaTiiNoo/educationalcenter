package com.epam.Repository.impl;

import com.epam.DTO.Student;
import com.epam.Repository.StudentRepository;


import java.util.ArrayList;
import java.util.List;

public class StudentRepositoryImpl implements StudentRepository {

    private List<Student> students = new ArrayList<>();

    @Override
    public void addStudent(Student student){
        students.add(student);
    }

    @Override
    public void updateStatusStudent(Student student){
        student.setStatus("Отчислить");
    }

    @Override
    public List<Student> getStudents() {
        return students;
    }
}
