package com.epam.Repository.impl;

import com.epam.DTO.Curriculum;
import com.epam.Repository.CurriculumRepository;

import java.util.ArrayList;
import java.util.List;

public class CurriculumRepositoryImpl implements CurriculumRepository {

    private List<Curriculum> curriculumList = new ArrayList<>();

    @Override
    public void createCurriculum(Curriculum curriculum){
        curriculumList.add(curriculum);
    }

    @Override
    public List<Curriculum> getCurriculumList() {
        return curriculumList;
    }
}
