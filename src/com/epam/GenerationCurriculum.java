package com.epam;

import com.epam.DTO.Course;
import com.epam.DTO.Curriculum;
import com.epam.DTO.Marks;
import com.epam.DTO.Student;
import com.epam.Service.CourseService;
import com.epam.Service.CurriculumService;
import com.epam.Service.StudentService;
import com.epam.Service.impl.CourseServiceImpl;
import com.epam.Service.impl.CurriculumServiceImpl;
import com.epam.Service.impl.StudentServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerationCurriculum {

    private StudentService studentService = new StudentServiceImpl();

    private CourseService courseService;

    private CurriculumService curriculumService = new CurriculumServiceImpl();

    private static final String[] courseName = {"VCS", "JavaBasic", "Exception", "IO", "Generics", "Collections", "Database", "JDBC", "Spring"};
    private static final String[] curriculumName = {"Java Developer ", "J2EE Developer"};
    private static final String[] studentName = {"Kolya", "Ivan", "Sasha", "Petya", "Vasya", "Ibragim"};

    private List<Course> generateCourses() {
        List<Course> courses = new ArrayList<>();
        int size = new Random().nextInt(9);
        for (int i = 0; i <= size; i++)
            courses.add(new Course(courseName[i], (size + 1) * 8));
        return courses;
    }

    private void generateCurriculum() {
        for (String curriculum : curriculumName) {
            courseService = new CourseServiceImpl();
            courseService.addCourses(generateCourses());
            curriculumService.createCurriculum(
                    new Curriculum(curriculum, LocalDate.now(), courseService.getCourses())
            );
        }
    }

    private List<Marks> generateMarks(int duration) {
        List<Marks> marks = new ArrayList<>();
        for (int i = 0; i <= new Random().nextInt(duration); i++) {
            marks.add(Marks.values()[new Random().nextInt(5)]);
        }
        return marks;
    }

    private void generateStudents() {
        generateCurriculum();
        for (String student : studentName) {
            int currIndex = new Random().nextInt(2);
            studentService.addStudent(
                    new Student(student,
                            curriculumService.getAllCurriculum().get(currIndex),
                            generateMarks(curriculumService.getAllCurriculum().get(currIndex).getCourses().stream().mapToInt(Course::getDuration).sum() / 8),
                            "Может продолжать обучение")
            );
        }
    }

    public StudentService getStudentService() {
        generateStudents();
        return studentService;
    }
}
