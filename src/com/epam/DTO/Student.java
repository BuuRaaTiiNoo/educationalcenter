package com.epam.DTO;

import com.epam.Service.impl.StudentServiceImpl;

import java.util.List;

public class Student {
    private String fullName;
    private Curriculum curriculum;
    private List<Marks> marks;
    private String status;

    public Student() {
    }

    public Student(String fullName, Curriculum curriculum, List<Marks> marks, String status) {
        this.fullName = fullName;
        this.curriculum = curriculum;
        this.marks = marks;
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public List<Marks> getMarks() {
        return marks;
    }

    public void setMarks(List<Marks> marks) {
        this.marks = marks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return fullName + " - До окончания обучения по программе " +
                curriculum.getCurriculum()
                + " осталось " + new StudentServiceImpl().getTimeLeft(this) + "д. Средний балл "
                + new StudentServiceImpl().middleMark(this)
                + " " + status;
    }
}
