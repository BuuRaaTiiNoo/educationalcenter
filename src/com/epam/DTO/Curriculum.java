package com.epam.DTO;

import java.time.LocalDate;
import java.util.List;

public class Curriculum {

    private String curriculum;
    private LocalDate startDate;
    private List<Course> courses;

    public Curriculum(String curriculum, LocalDate startDate, List<Course> courses) {
        this.curriculum = curriculum;
        this.startDate = startDate;
        this.courses = courses;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Curriculum{" +
                "curriculum='" + curriculum + '\'' +
                ", startDate=" + startDate +
                ", courses=" + courses +
                '}';
    }
}
