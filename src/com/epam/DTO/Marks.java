package com.epam.DTO;

public enum Marks {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5);
    private final int value;

    public int getValue() {
        return value;
    }

    Marks(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value + "";
    }
}
