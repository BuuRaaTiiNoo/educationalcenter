package com.epam;

import com.epam.Service.StudentService;


public class Main {

    private static GenerationCurriculum generationCurriculum = new GenerationCurriculum();

    public static void main(String[] args) {

        StudentService studentServiceImpl = generationCurriculum.getStudentService();

        studentServiceImpl.checkStatusStudents();

        System.out.println("Список студентов отсортированных по рейтингу:");
        studentServiceImpl.getSortedListStudentByRating();

        System.out.println("\nСписок студентов отсортированных по времени до конца обучения:");
        studentServiceImpl.getSortedListStudentByTimeLeft();

        System.out.println("\nЕсть вероятность, что не будет отчислен:");
        studentServiceImpl.getFilterStudent();
    }
}
